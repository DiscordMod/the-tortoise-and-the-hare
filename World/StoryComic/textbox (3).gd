extends CanvasLayer

@onready var textbox_container = $"."
@onready var start_symbol = $"./MarginContainer2/HBoxContainer/StartingText"
@onready var end_symbol = $"./MarginContainer2/HBoxContainer/ChangePanelButton" 
@onready var text_contents = $"./MarginContainer2/HBoxContainer/ActualTextContent"
@onready var timer = $MarginContainer2/HBoxContainer/Timer
@onready var tween = get_tree().create_tween()
@onready var first_dialogue = "we’ll see about that"

var dialogue_array = ["yes ! i made it",
					"oh no ! i’ll have to try again", 
					"hahah! See, i told you were only a little loser"]

# Called when the node enters the scene tree for the first time.
func _ready():
	hide_textbox()
	display_text(text_contents, first_dialogue)

func hide_textbox():
	start_symbol.text = ""
	end_symbol.text = ""
	text_contents.text = ""
	textbox_container.hide()

func show_textbox():
	start_symbol.text = "*"
	textbox_container.show()
	end_symbol.text = ">"

# fix the visible ratio because it's going backwards + add when the tween is done with its animation, show the end symbol

func display_text(text_content, next_text):
	text_content.text = next_text
	show_textbox()
	tween.tween_property(text_content, "visible_ratio", 1.0, 0.5)


func next_dialog() -> void:
	if Input.is_action_just_released("move_right"):
		if count < 3:
			display_text(text_contents, dialogue_array[count])
			print(count)
			count +=1

var count = 0
func _on_timer_timeout():
	tween.tween_property(text_contents, "visible_ratio", 0.0, 0.0)
	if count < 3:
		display_text(text_contents, dialogue_array[count])
		count += 1
