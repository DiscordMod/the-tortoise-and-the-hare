extends Node2D

@onready var Player = $Player
@onready var Boss = $Boss

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Boss.ifgone:
		Boss.queue_free()
		get_tree().change_scene_to_file("res://World/home_screen.tscn")
	if Player.ifgone:
		Player.queue_free()
		get_tree().change_scene_to_file("res://World/main.tscn")
