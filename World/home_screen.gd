extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


# will change the scene once it presses start button
func _on_button_pressed():
	get_tree().change_scene_to_file("res://World/StoryComic/storypanel1.tscn")

# will quit the game once the button is pressed
func _on_button_2_pressed():
	get_tree().quit()
