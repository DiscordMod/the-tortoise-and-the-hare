extends Area2D

@export var speed: int = 1000

func _physics_process(delta):
	var direction = Vector2.RIGHT.rotated(rotation)
	global_position += speed * direction * delta
	
func destroy():
	queue_free()
	
func _on_PlayerShuriken_area_entered(area):
	destroy()
	
func collide_with_platform():
	destroy()
	
func _on_PlayerShuriken_body_entered(body):
	destroy()
	
func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

