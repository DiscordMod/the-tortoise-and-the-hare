extends Node2D

@export var Main: PackedScene = preload("res://Actions/boss_attack.tscn")

var max_hp: int = 500
var hp: int = max_hp

var cooldown = 0
var max_cooldown = 200

var rng = RandomNumberGenerator.new()

@export var ifgone: bool = false


func first_attack():
	var attack_position = [Vector2(200, -100), Vector2(730, -100), Vector2(1190, -100), Vector2(1720, -100)]
	if Main and cooldown < 0:
		for index in attack_position.size():
			var main = Main.instantiate()
			get_tree().current_scene.add_child(main)
			main.global_position = self.get_parent().global_position + attack_position[index]
			cooldown = max_cooldown
			main.rotation = PI/2
func second_attack():
	var attack_position = [Vector2(465, -100), Vector2(960, -100), Vector2(1455, -100)]
	if Main and cooldown < 0:
		for index in attack_position.size():
			var main = Main.instantiate()
			get_tree().current_scene.add_child(main)
			main.global_position = self.get_parent().global_position + attack_position[index]
			cooldown = max_cooldown
			main.rotation = PI/2
func third_attack():
	#var attack_position = [Vector2(2020, Player.curr_position)]
	var attack_position = [Vector2(2020, 560), Vector2(2020, 920)]
	if Main and cooldown < 0:
		for index in attack_position.size():
			var main = Main.instantiate()
			get_tree().current_scene.add_child(main)
			main.global_position = self.get_parent().global_position + attack_position[index]
			cooldown = max_cooldown
			main.rotation = PI
func fourth_attack():
	var attack_position = [Vector2(2020, 740)]
	if Main and cooldown < 0:
		for index in attack_position.size():
			var main = Main.instantiate()
			get_tree().current_scene.add_child(main)
			main.global_position = self.get_parent().global_position + attack_position[index]
			cooldown = max_cooldown
			main.rotation = PI
func fifth_attack():
	pass

func manage_attack():
	var random_num = rng.randi_range(0, 5)
	if random_num == 0: first_attack()
	if random_num == 1: second_attack()
	if random_num == 2: third_attack()
	if random_num == 3: fourth_attack()
	if random_num == 4: fifth_attack()
	pass

func _on_hurtbox_area_entered(hitbox):
	var parent_node = hitbox.get_parent()
	var base_damage = hitbox.damage
	self.hp -= base_damage
	parent_node.destroy()
	
func _process(delta):
	manage_attack()
	cooldown-=1
	if hp < 0:
		ifgone = true
