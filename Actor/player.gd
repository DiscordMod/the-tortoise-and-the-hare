extends CharacterBody2D

@export var Main: PackedScene = preload("res://Actions/main_attack.tscn")

var curr_position = self.global_position

var DEFAULT_GRAVITY: Vector2 = Vector2(0, 1).rotated(deg_to_rad(0.0))
var CURRENT_SPEED: Vector2 = Vector2(0, 0).rotated(deg_to_rad(0.0))
var isTouchingGround: bool = false
var hasDoubleJump: bool = true
var TIMES: int = 0
var speedx: int = 450
var speedy: int = 600
var speedy_dj: int = 500
var max_cooldown: int = 50
var cooldown:int  = 0
var lastDirection = "Right"
var max_hp: int = 30
var hp: int = max_hp
@export var ifgone: bool = false

func handleMovementInput():
	if (!isTouchingGround):
		TIMES+=1
		CURRENT_SPEED += DEFAULT_GRAVITY*(TIMES)
	else:
		TIMES = 0
	var moveDirection = Input.get_vector("move_left", "move_right", "null", "null")
	var move_x = moveDirection.x * speedx
	if Input.is_action_just_pressed("drop"):
		TIMES += 100
	if ((hasDoubleJump or isTouchingGround) and Input.is_action_just_pressed("jump")):
		if hasDoubleJump and !isTouchingGround:
			hasDoubleJump = false
			CURRENT_SPEED = Vector2(0, -1) * speedy_dj
		elif hasDoubleJump and isTouchingGround:
			CURRENT_SPEED = Vector2(0, -1) * speedy
		TIMES = 0
	
	var move_y = CURRENT_SPEED.y
	if (move_x < 0) : lastDirection = "Left"
	if (move_x > 0) : lastDirection = "Right"
	velocity = Vector2(move_x, move_y)
	
	move_and_slide()
	if is_on_ceiling():
		CURRENT_SPEED = Vector2.ZERO
	if is_on_floor(): 
		isTouchingGround = true
		hasDoubleJump = true
	else:
		isTouchingGround = false

func handleShooting():
	if Input.is_action_pressed("shoot"):
		Shooting()

func Shooting():
	if Main and cooldown < 0:
		var main = Main.instantiate()
		get_tree().current_scene.add_child(main)
		main.global_position = self.global_position + Vector2(0, -60)
		cooldown = max_cooldown
		
		if lastDirection == "Left":
			main.rotation = PI
		elif lastDirection == "Right":
			main.rotation = 0

func _physics_process(delta):
	handleMovementInput()
	handleShooting()
	
func _process(delta):
	curr_position = self.global_position
	cooldown-=1
	if self.hp < 0:
		ifgone = true
		#queue_free()

func _on_hurtbox_area_entered(hitbox):
	var parent_node = hitbox.get_parent()
	var base_damage = hitbox.damage
	self.hp -= base_damage
	parent_node.destroy()
